\documentclass[10.5pt,oneside,a4paper]{article}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage[margin=1in,tmargin=0.5in]{geometry}
\usepackage{listings}
\usepackage{float}
\usepackage{tikz}
\usepackage[utf8]{inputenc}

\usetikzlibrary{shapes.geometric, arrows}
\graphicspath{ {images/} }


\usepackage[
    backend=biber,
    style=authoryear-icomp]{biblatex}
\addbibresource{os.bib}
\usepackage[]{hyperref}
\hypersetup{
    colorlinks=true,
}

\title{
    FreeRTOS: Real-Time Operating System \\
    \large Operating Systems CA2
    }
\author{
    Oshi Kavadia \\ 
    \href{mailto:D00169990@student.dkit.ie}{D00169990@student.dkit.ie}
    } 
\date{}
 
\begin{document}
    \maketitle
    \begin{abstract}
        FreeRTOS is a small, scalable RTOS. Designed to be small and portable, it powers a number of embedded solutions. It provides a dynamic and priority based scheduler, along with customisable memory allocation implementations.

        It can be setup to run as a hard RTOS, with low jitter or a soft RTOS; Depending on the developers' needs.
    \end{abstract}
    \section{Introduction}
    \begin{wrapfigure}{H}{0.2\textwidth}
        \centering
        \includegraphics[width=0.2\textwidth]{logo.jpg}
        \caption{FreeRTOS Logo}    
    \end{wrapfigure}
    A Real Time Operating System (RTOS) is an Operating System specialized for real time operations
    To be classified as an RTOS, a system must be deterministic and have response time predictability \parencite{chibiosRtos}. Real time systems are designed to do something within a certain amount of time; they guarantee that things happen when they are supposed to. Pacemakers are a good example of a real time system, they must contract the heart muscle at the right time, it can't be too busy to respond in time \parencite{TheArchi64:online}

    FreeRTOS is an open source, scale-able real time operating system kernel, released under the MIT license. It is developed and maintained by Real Time Engineers Ltd. It focuses on embedded devices and has been ported to 35 micro-controllers. 
    The kernel is designed to be small and simple, consisting of only 3 C files \parencite{FreeRTOS66:online}.
    \section{Technical Overview}
        \subsection{Architecture}
        As stated above, FreeRTOS is a relatively small kernel, minimum core totalling just under 9000 lines of code. Its code can be broken into three main areas, tasks, communications and hardware interfacing.
        \begin{itemize}
            \item Tasks - About 50 Percent of the core kernel deals with tasks. A task is a user-defined C function with a given priority.
            \item Communication - queue.c and queue.h handle FreeRTOS communication. Tasks and interrupts use queues to send data and to signal the use of critical resources using mutexes and semaphores.
            \item Hardware Interfacing - Majority of the kernel is hardware independent. Only a small fraction of the code (about 6\%) acts as the bridge between the hardware independent and hardware dependent code.
            \begin{figure}[h]
                \centering
                \includegraphics[width=0.4\textwidth]{layers.png}
                \caption{Software layers, \parencite{TheArchi64:online}}    
            \end{figure}
        \end{itemize}
        Compared to Windows NT or UNIX-like Operating Systems, FreeRTOS is used in very dedicated roles such as extremely precise control system where decisions/calculations must be completed in a very exacting time frame.
    \section{Process Management}
    FreeRTOS uses dynamic and priority-based model. The task with the highest priority is granted CPU time. If multiple tasks have equal priority, a Round Robin algorithm is used among them.

    The scheduler can be preemptive or cooperative. Cooperative scheduling does not implement a timer based scheduler decision point, processes pass control to one another by yielding. The scheduler interrupts at regular frequency simply to increment the tick count. The scheduling mode between preemptive and cooperative is decided in a configuration switch.

    Deadlocks are avoided by forcing all blocking processes to timeout with the result that application developers are required to set. Developers are also required to tune timeouts and deal with resource allocation failures \parencite{goyette2007analysis}.
    Interprocess communication is achieved via message queues and basic binary semaphores.

    Critical section processing is handled by disabling of interrupts. Critical sections within a task can be nested and each task tracks its own nesting count.

    The scheduler can be suspended when exclusive access to the microcontroller unit is required without jeopardising the operation of interrupt service routine.
    \begin{figure}[h]
        \centering
        \includegraphics[width=0.6\textwidth]{RTExample.png}
        \caption{Example RTOS Scheduling}
        \label{fig:sched}
    \end{figure}
    
    Figure \ref{fig:sched} shows a scheduling example where \emph{`vControlTask'} has the highest priority, \emph{`vKeyHandlerTask'} has medium priority, and \emph{`idle task'} runs where there is no other task.
    
    \section{Memory Management}
    The FreeRTOS kernel has to allocate RAM each time a task, queue, timer, mutex, or semaphore is created. The RAM can be automatically dynamically allocated from the RTOS heap within the RTOS API object creation function, or starting from FreeRTOS V9.0.0 \parencite{FreeRTOSMem:online} the application developer has the ability to instead provide the memory themselves, allowing some objects (Tasks, Queues, Mutexes, Binary/Counting/Recursive Sephamores, Timers and Event groups) to optionally be created without any memory being allocated dynamically.

    The decision to use either static or dynamic memory allocation is dependent on each application and the developers' personal preference. Both methods have pros and cons, and an application can feature both approaches at the same time.
    
    If RTOS objects are created dynamically using standard C \emph{malloc()} and \emph{free()} functions, they might be accompanied with certain undesirable side effects for some of the following reasons \parencite{jadaan_2015}:
    \begin{itemize}
        \item Implementation can be relatively large, taking up valuable code space.
        \item Rarely thread safe
        \item Not deterministic; the amount of time taken to execute the functions will differ from call to call.
        \item Can suffer from memory fragmentation
        \item Can complicate the linker configuration
    \end{itemize}
    FreeRTOS defines two new functions to provide an alternative memory allocation implementation to avoid some of the stated problems.

    FreeRTOS provides five sample memory allocation implementations. Only one of these implementations (provided in separate header files), can be included in a project.
    \begin{table}
    \begin{center} 
        \begin{tabular}{ |c|c|c|c|p{5cm}| }
        \hline
         Implementation & Deterministic & Code Size & Segmentation & Notes \\
         \hline \hline
         heap\_1 & Yes & Small & No & the very simplest, does not permit memory to be freed \\ 
         \hline
         heap\_2 & No  & Small & High & permits memory to be freed, but not does coalescence adjacent free blocks.\\
         \hline
         heap\_3 & No  & Large & Target-dependent & simply wraps the standard malloc() and free() for thread safety\\
         \hline
         heap\_4 & No  & Moderate & Moderate & coalescences adjacent free blocks to avoid fragmentation. Includes absolute address placement option \\
         \hline
         heap\_5 & No  & Moderate & Low & as per heap\_4, with the ability to span the heap across multiple non-adjacent memory areas \\
         \hline
        \end{tabular}
        \caption{Comparison of provided allocation implementations}
    \end{center}
    \end{table}


   
    \begin{figure}[h]
        In figure \ref{fig:heap}:
        \begin{itemize}
            \item A shows the free array before any tasks have been created
            \item B shows the array after one task has been created
            \item C shows the array after three tasks have been created
        \end{itemize}
        \centering
        \includegraphics[width=0.47\textwidth]{heap1.png}
        \caption{The figure shows how heap\_1 subdivides the simple array as tasks are created, \parencite{HeapAWS}} 
        \label{fig:heap}   
    \end{figure}
    \section{Persistence}  
    FreeRTOS has support for a number of file system for varied use cases. Apart from the file systems supported by the maintainers, there's also 3rd party solutions that can be used by application developers if desired \parencite{Coffeefi71:online}.
    \subsection{FAT}
    FreeRTOS+FAT is an open source and scalable implementation of the FAT file system. It was recently acquired by Real Time Engineers Ltd for use with and without an RTOS \parencite{FreeRTOS7:online}. 

    Although in development, It is already used in a number of commercial projects. It provides a standard C library style API and includes a thread local error number value and detailed error codes. 
    
    It is fully thread aware, Supports FAT12/FAT16/FAT32, optional long file name, and directory name hashing for speed.

    \subsection{Flash Based}
    Reliance Edge is Datalight's portable, open source, and fail-safe transactional file system. It is also MIRSA (Motor Industry Software Reliability Association) compliant \parencite{Powerfai45:online}.

    It protects critical data from corruption, even when power failures occur. It is designed specifically for small and resource-constrained embedded systems. It can use as little as 4K bytes of RAM and 12K bytes of code space if configured properly.

    Some of its key features are:
    \begin{itemize}
        \item Reliable operation does not require media having atomic sector writes
        \item Can coexist with other file systems; or replace them entirely
        \item Supports NAND/NOR flash, eMMC, NVRAM. SATA/PATA etc.
    \end{itemize}
    \section{Technical Feature}
    A key technical feature of any RTOS is consistency concerning the amount of time it takes to accept and complete an application's task. This variability is called jitter. Jitter is a significant and usually undesired factor in the design of almost all communications link.

    RTOS can usually be divided into three categories : 
    \begin{itemize}
        \item Hard RTOS - Missing a deadline is a system failure.
        \item Firm RTOS - Infrequent deadline misses are tolerable, but may degrade the systems QoS.
        \item Soft RTOS - The usefulness of a result degrades after it's deadline, thereby degrading the systems Qos.
    \end{itemize}
    Put simply, if an RTOS can generally meet a deadline, it's a soft/firm RTOS. But if it can meet its deadline deterministically, it's a hard RTOS. A hard RTOS has less jitter than a soft RTOS.

    Systems used for many mission critical applications must be real-time (Anti-Lock braking system in cars, controls of an aircraft). Real time processing fails if not completed within a specified deadline relative to an event.
    \printbibliography 
\end{document} 
