	<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"lang="en" dir="ltr" class="no-js">
  
<head>
<!-- Begin Cookie Consent plugin -->
		 <script type="text/javascript">
		 window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"","link":null,"theme":"dark-top"};
		 </script>
		<script type="text/javascript" src="//s3.amazonaws.com/cc.silktide.com/cookieconsent.latest.min.js"></script>
        <!-- End Cookie Consent plugin --><meta charset="UTF-8" /><link href="http://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet" type="text/css"/><link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/><link href="http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold" rel="stylesheet" type="text/css"/><meta name="HandheldFriendly" content="true" /><meta name="viewport" content="width=320px, initial-scale=1.0"/><link rel="stylesheet" media="only screen and (max-width: 360px)" type="text/css" href="/dokuwiki/lib/tpl/chibios/css/screen_360.css"/><link rel="stylesheet" media="only screen and (min-width: 361px) and (max-width: 640px)" type="text/css" href="/dokuwiki/lib/tpl/chibios/css/screen_640.css"/><title>ChibiOS free embedded RTOS - RTOS Concepts</title><link rel="icon" type="image/png" sizes="192x192" href="/dokuwiki/lib/tpl/chibios/images/logos/slogo.png"/><script>(function(H){H.className=H.className.replace(/\bno-js\b/,"js")})(document.documentElement)</script><script src="/dokuwiki/lib/tpl/chibios/js/common/modernizr.js"></script><meta name="generator" content="DokuWiki"/>
<meta name="robots" content="index,follow"/>
<meta name="keywords" content="chibios,articles,rtos_concepts"/>
<link rel="search" type="application/opensearchdescription+xml" href="/dokuwiki/lib/exe/opensearch.php" title="ChibiOS free embedded RTOS"/>
<link rel="start" href="/dokuwiki/"/>
<link rel="contents" href="/dokuwiki/doku.php?id=chibios:articles:rtos_concepts&amp;do=index" title="Sitemap"/>
<link rel="alternate" type="application/rss+xml" title="Recent changes" href="/dokuwiki/feed.php"/>
<link rel="alternate" type="application/rss+xml" title="Current namespace" href="/dokuwiki/feed.php?mode=list&amp;ns=chibios:articles"/>
<link rel="alternate" type="text/html" title="Plain HTML" href="/dokuwiki/doku.php?do=export_xhtml&amp;id=chibios:articles:rtos_concepts"/>
<link rel="alternate" type="text/plain" title="Wiki Markup" href="/dokuwiki/doku.php?do=export_raw&amp;id=chibios:articles:rtos_concepts"/>
<link rel="canonical" href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:articles:rtos_concepts"/>
<link rel="stylesheet" type="text/css" href="/dokuwiki/lib/exe/css.php?t=chibios&amp;tseed=58b50655c272f5e8ccb06a35dc20f7b6"/>
<script type="text/javascript">/*<![CDATA[*/var NS='chibios:articles';var JSINFO = {"id":"chibios:articles:rtos_concepts","namespace":"chibios:articles"};
/*!]]>*/</script>
<script type="text/javascript" charset="utf-8" src="/dokuwiki/lib/exe/js.php?tseed=58b50655c272f5e8ccb06a35dc20f7b6"></script>
</head>

<body>

<div id="dokuwiki__top" class="site dokuwiki mode_show tpl_chibios    
	">
        </div>

<div id="ch_site_wrapper">
	<div id="ch_top">
		<div id="ch_logo" class="left">
			<a class="big_mid" href="" target="_blank"><img src="/dokuwiki/lib/tpl/chibios//images/logos/new_logo.png"></a><a class="small" href="" target="_blank"><img src="/dokuwiki/lib/tpl/chibios//images/logos/new_logo_textonly.png"></a>		
		</div>
		<div id="ch_ads" class="big right">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Main pages, top, 728x90, created 9/18/10 -->
			  <ins class="adsbygoogle"
			  style="display:inline-block;width:728px;height:90px"
			  data-ad-client="ca-pub-3840594581853944"
			  data-ad-slot="5653904444"></ins><script>
		 (adsbygoogle = window.adsbygoogle || []).push({});
		 </script>		</div>
		<div id="ch_ads" class="mid_small right responsive">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Main pages, top, responsive -->
			  <ins class="adsbygoogle"
			   style="display:block"
			   data-ad-client="ca-pub-3840594581853944"
			   data-ad-slot="8233408323"
               data-ad-format="auto"></ins><script>
		 (adsbygoogle = window.adsbygoogle || []).push({});
		 </script>		</div>			
	</div>
	<div id="ch_tophornav" class="left big">
			<ul class="nopoint hor"><li class="home"><a href="http://www.chibios.org/dokuwiki/"></a><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:product:start">Products</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:downloads:start">Downloads</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:documentation:start">Documentation</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:articles:start">Articles</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:licensing:start">Licensing</a></li><div id="ch_social" class="right"><a href="https://www.facebook.com/pages/ChibiOSRT/219674618230788" target="_blank"><img src="/dokuwiki/lib/tpl/chibios/images/buttons/social_fb_40.png"></a><a href="https://twitter.com/ChibiOS_RT" target="_blank"><img src="/dokuwiki/lib/tpl/chibios/images/buttons/social_tw_40.png"></a><a href="https://www.linkedin.com/company/chibios-rt" target="_blank"><img src="/dokuwiki/lib/tpl/chibios/images/buttons/social_in_40.png"></a></div><div id="ch_search"><form method="get" action="http://www.google.com/custom" class="search"  id="dw__search" target="_top"><div class="no"><label for="sbi" style="display: none">Enter your search terms</label><input type="text" name="q" size="31" maxlength="255" placeholder="Search..."value="" id="sbi"></input><input type="hidden" name="domains" value="chibios.org"></input><input type="hidden" name="sitesearch" value="chibios.org"></input><label for="sbb" style="display: none">Submit search form</label><input type="hidden" name="client" value="pub-0981582256507874"></input><input type="hidden" name="forid" value="1"></input><input type="hidden" name="ie" value="ISO-8859-1"></input><input type="hidden" name="oe" value="ISO-8859-1"></input><input type="hidden" name="safe" value="active"></input><input type="hidden" name="cof" value="GALT:#008000;GL:1;DIV:#336699;VLC:663399;AH:center;BGC:FFFFFF;LBGC:336699;ALC:0000FF;LC:0000FF;T:000000;GFNT:0000FF;GIMP:0000FF;FORID:1"></input><input type="hidden" name="hl" value="en"></input></div></form></div></ul>	</div>
	<div id="ch_topvernav" class="left mid_small">
         <button class="cmn-toggle-switch cmn-toggle-switch__htx">
            <span>toggle menu</span>
         </button>
		<ul class="nopoint ver navbar"><li class="home"><a href="http://www.chibios.org/dokuwiki/"></a><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:product:start">Products</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:downloads:start">Downloads</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:documentation:start">Documentation</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:articles:start">Articles</a></li><li><a href="http://www.chibios.org/dokuwiki/doku.php?id=chibios:licensing:start">Licensing</a></li></ul>	</div>
	<div id="ch_middlepage">		
				<div id="ch_side" class="left">
			<button class="cmn-toggle-switch cmn-toggle-switch__htla mid_small">
				<span>toggle menu</span>
			</button>
			<span class="left background">
			<div id="ch_sidenavbar" class="left">
				<div class="wrap_sidebar plugin_wrap">
<h5 id="product_information">Product Information</h5>
<ul>
<li class="level1"><div class="li"> <a href="/dokuwiki/doku.php?id=chibios:product:start" class="wikilink1" title="chibios:product:start">Products</a></div>
<ul>
<li class="level2"><div class="li"> <a href="/dokuwiki/doku.php?id=chibios:product:rt:start" class="wikilink1" title="chibios:product:rt:start">RT</a></div>
</li>
<li class="level2"><div class="li"> <a href="/dokuwiki/doku.php?id=chibios:product:nil:start" class="wikilink1" title="chibios:product:nil:start">NIL</a></div>
</li>
<li class="level2"><div class="li"> <a href="/dokuwiki/doku.php?id=chibios:product:hal:start" class="wikilink1" title="chibios:product:hal:start">HAL</a></div>
</li>
<li class="level2"><div class="li"> <a href="/dokuwiki/doku.php?id=chibios:product:chibistudio:start" class="wikilink1" title="chibios:product:chibistudio:start">ChibiStudio</a></div>
</li>
</ul>
</li>
</ul>

<h5 id="community_and_support">Community and Support</h5>
<ul>
<li class="level1"><div class="li"> <a href="https://sourceforge.net/projects/chibios" class="urlextern" target="_blank" title="https://sourceforge.net/projects/chibios"  rel="nofollow">SourceForge Page</a></div>
</li>
<li class="level1"><div class="li"> <a href="http://wiki.chibios.org" class="urlextern" target="_blank" title="http://wiki.chibios.org"  rel="nofollow">Technical Wiki</a></div>
</li>
<li class="level1"><div class="li"> <a href="http://forum.chibios.org" class="urlextern" target="_blank" title="http://forum.chibios.org"  rel="nofollow">Support Forum</a></div>
</li>
<li class="level1"><div class="li"> <a href="http://wiki.chibios.org/dokuwiki/doku.php?id=chibios:support#online_help" class="urlextern" target="_blank" title="http://wiki.chibios.org/dokuwiki/doku.php?id=chibios:support#online_help"  rel="nofollow">IRC Channel</a></div>
</li>
</ul>
</div>
			</div>
			<div id="ch_sideads" class="big left">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Main pages, left, 200x200, created 10/9/10 -->
			 <ins class="adsbygoogle"
			  style="display:inline-block;width:200px;height:200px"
			  data-ad-client="ca-pub-3840594581853944"
			  data-ad-slot="6448204779"></ins><script>
		 (adsbygoogle = window.adsbygoogle || []).push({});
		 </script>			</div>
			<div id="ch_stat" class="big left">
				<span style="display:none">  <!-- Inizio Codice Shinystat -->
			<script type="text/javascript" language="JavaScript"
			src="http://codice.shinystat.com/cgi-bin/getcod.cgi?USER=ChibiOS">
			</script>
			<noscript><a href="http://www.shinystat.com/it" target="_top">
			<img src="http://www.shinystat.com/cgi-bin/shinystat.cgi?USER=ChibiOS"
			alt="Statistiche" border="0">
			</a></noscript>
			<!-- Fine Codice Shinystat -->
			</span>			</div>
			</span>
		</div>
				<div id="ch_content" class="left with_sidenavbar">
			<div class="wrap_middle plugin_wrap">
<h1 class="sectionedit3" id="rtos_concepts">RTOS Concepts</h1>

<p>
This is a brief introduction to basic Real Time Operating Systems concepts.
</p>

<h4 id="what_is_an_rtos">What is an RTOS</h4>

<p>
An RTOS is an operating system specialized for real time operations. In order to be classifiable as an RTOS an operating system must:
</p>
<ul>
<li class="level1"><div class="li"> Have response time <strong>predictability</strong>.</div>
</li>
<li class="level1"><div class="li"> Be <strong>deterministic</strong>.</div>
</li>
</ul>

<p>
Other qualities like speed, features set, small size etc, while important, are not what really characterize an RTOS.
</p>

<h4 id="systems_classification">Systems Classification</h4>

<p>
Any system can classified in one of the following categories.
</p>

<h5 id="non_real_time_systems">Non Real Time systems</h5>

<p>
A non real time system is a system where there are no deadlines involved. Non-RT systems could be described as follow:
</p>

<p>
“<em>A non real time system is a system where the programmed reaction to an event will certainly happen sometime in the future</em>”.
</p>

<h5 id="soft_real_time_systems">Soft Real Time systems</h5>

<p>
A Soft real time system is a system where not meeting a deadline can have undesirable but not catastrophic effects, a performance degradation for example. SRTs could be described as follow:
</p>

<p>
“<em>A soft real time system is a system where the programmed reaction to an event is almost always completed within a known finite time</em>”.
</p>

<h5 id="hard_real_time_systems">Hard Real Time systems</h5>

<p>
An Hard Real Time (HRT) system is a system where not meeting a deadline can have catastrophic effects. HRT systems require a much more strict definition and could be described as follow:
</p>

<p>
“<em>An hard real time system is a system where the programmed reaction to an event must be guaranteed to be completed within a known finite time</em>”.
</p>

<h5 id="considerations">Considerations</h5>

<p>
As you can see speed is not the main factor, predictability and determinism are. It is also important to understand that it is not the RTOS that makes a system SRT or HRT but the system design itself, the RTOS is just a tool that you can use in the right or wrong way.
</p>

<p>
Note that both SRT and HRT processes could coexist within the same system, even non critical processes without any RT constraints could be included in a design.
</p>

<h4 id="scheduling_states_and_priorities">Scheduling, States and Priorities</h4>

<p>
Most RTOSs, including ChibiOS/RT and ChibiOS/NIL, implement “fixed priority preemptive” scheduling algorithm. The strategy is very simple and can be described using few rules:
</p>
<ul>
<li class="level1"><div class="li"> Each thread has its own priority level, priorities are fixed and do not change unless the system is specifically designed to do so.</div>
</li>
<li class="level1"><div class="li"> Each <em>active</em> thread can be in one of the following states:</div>
<ul>
<li class="level2"><div class="li"> <strong>Running</strong>, currently being executed by a physical core.</div>
</li>
<li class="level2"><div class="li"> <strong>Ready</strong>, ready to be executed when a physical core will become available.</div>
</li>
<li class="level2"><div class="li"> <strong>Waiting</strong>, not ready for execution because waiting for an external event. Most RTOSes split this state in several sub-states but those are still waiting states.</div>
</li>
</ul>
</li>
<li class="level1"><div class="li"> Each physical core in the system always executes the highest priority thread that is ready for execution.</div>
</li>
<li class="level1"><div class="li"> When a thread becomes ready and there is a lower priority thread being executed then preemption occurs and the higher priority thread is executed, the lower priority thread goes in the ready state.</div>
</li>
</ul>

<p>
If the system has N cores the above strategy ensures that the N highest priority threads are being executed in any moment. Small embedded systems usually have a single core so there is only one running thread in any moment.<br/>

An explanation of how priorities are organized in ChibiOS/RT can be found in the article “<a href="/dokuwiki/doku.php?id=chibios:kb:priority" class="wikilink1" title="chibios:kb:priority">Priority Levels</a>”.
</p>

<h4 id="interrupts_handling">Interrupts handling</h4>

<p>
An important role of an <em>embedded</em> RTOS is handling of interrupts. Interrupts are an important events source to which a system designed around an RTOS is supposed to react. We can classify interrupt sources in two main classes:
</p>
<ul>
<li class="level1"><div class="li"> RTOS-related interrupt sources. This class of interrupts are required to interact with the RTOS in order to wakeup threads waiting for external events.</div>
</li>
<li class="level1"><div class="li"> Non RTOS-related interrupt sources. Interrupt sources that do not need to interact with the RTOS. This class of interrupts could also be able to preempt the kernel in those architectures supporting maskable priority levels (ARM Cortex-M3) or separate interrupt lines (ARM7).</div>
</li>
</ul>

<p>
A carefully designed RTOS should implement mechanisms efficiently handling the synchronization between threads and interrupt sources. Flexibility is important at this level, the capability to wake up single or multiple threads, synchronously or asynchronously is very valuable. On the other side threads should be able to wait for a single or multiple events.<br/>

Usually interrupt events are abstracted in a RTOS using mechanism like semaphores, event flags, queues or others, there is much variability in how this is implemented by the various RTOSes.
</p>

<h4 id="do_i_need_an_rtos">Do I need an RTOS?</h4>

<p>
It depends, you don&#039;t have to use an RTOS in order to design a predictable system but an RTOS offers you a methodology that can allow you to design a predictable system, without an RTOS you are basically on your own. Note that this methodology is not necessarily the “priority based multitasking” as implemented by ChibiOS/RT and many other RTOSes, this is just the most common scheme.
</p>

<p>
You may not need extreme predictability in your system but still want to use an RTOS simply because it can be convenient to use compared to a bare metal system. An RTOS, especially one designed for embedded applications, can also offer other services like, for example, a stable runtime environment, device drivers, file systems, networking and other useful subsystems.
</p>

<h4 id="what_makes_for_a_good_rtos">What makes for a good RTOS?</h4>

<p>
Assuming that all the candidates can be classified as RTOSs having the mentioned minimum requirements, then are all the other features that make the difference. Usually some specific features or measurable parameters are highly regarded in RTOSs.
</p>

<h5 id="response_time">Response Time</h5>

<p>
An important parameter when evaluating an RTOS is its response time. An efficient RTOS only adds a small overhead to the system theoretical minimal response time. Typical parameters falling in this category are:
</p>
<ul>
<li class="level1"><div class="li"> <strong>Interrupt latency</strong>, the time from an interrupt request and the interrupt servicing. An RTOS can add some overhead in interrupt servicing. The overhead can be caused by extra code inserted by the RTOS into the interrupt handlers code paths or by RTOS-related critical zones.</div>
</li>
<li class="level1"><div class="li"> <strong>Threads fly-back time</strong>, the time from an hardware event, usually an interrupt, and the restart of the thread supposed to handle it.</div>
</li>
<li class="level1"><div class="li"> <strong>Context switch time</strong>, the time required to synchronously switch from the context of one thread to the context of another thread.</div>
</li>
</ul>

<p>
Of course an RTOS capable to react within 2µS is better than a system that reacts within 10µS. Note that what is really meaningful is the worst case value, if a system reacts in average in 5µS but, because jitter, can have spikes up to 20µS then the value to be considered is 20µS.
</p>

<h5 id="jitter">Jitter</h5>

<p>
A good RTOS is also characterized by low intrinsic jitter in response time. Intrinsic because jitter is also determined by the overall system design. Some factors that determine the system behavior regarding jitter are:
</p>
<ul>
<li class="level1"><div class="li"> Thread priorities assignment.</div>
</li>
<li class="level1"><div class="li"> Interrupt priorities assignment.</div>
</li>
<li class="level1"><div class="li"> Length and number of critical zones.</div>
</li>
<li class="level1"><div class="li"> Interactions between threads through shared resources protected by mutual exclusion.</div>
</li>
<li class="level1"><div class="li"> Use of priority inheritance or other jitter-reducing algorithms/strategies.</div>
</li>
</ul>

<p>
See the article “<a href="/dokuwiki/doku.php?id=chibios:articles:jitter" class="wikilink1" title="chibios:articles:jitter">Response Time and Jitter</a>”.
</p>

<h5 id="size">Size</h5>

<p>
In an embedded system the RTOS is an important overhead in terms of occupied memory, a more compact RTOS is preferable being all the other parameters equal because memory cost.
</p>

<h5 id="reliability">Reliability</h5>

<p>
There are design choices that make some systems intrinsically more reliable that others. Dynamic allocation is a good example of a poor design choice because both unreliability and response time unpredictability of some allocation schemes. Fully static designs do not have those intrinsic limitations.
</p>

<h5 id="synchronization_primitives">Synchronization Primitives</h5>

<p>
Variety in available primitives is also an important factor to be considered. Having the correct tool for the job can reduce development time and often also helps when integrating external code with the RTOS.
</p>

<p>
A good example is the lwIP TCP/IP stack, it assumes an RTOS offering semaphores with timeouts, if your RTOS does not support semaphores <em>and</em> timeouts then you have a problem and will have to find a workaround.
</p>
</div><div class="wrap_side plugin_wrap"><div class="plugin_include_content plugin_include__private:support" id="plugin_include__private__support">
<div class="level0">
<div class="wrap_round wrap_box plugin_wrap">
<h4 id="more_articles_and_guides">More Articles and Guides</h4>

<p>
More articles and guides are available on the technical wiki.
</p>

<p>
<a href="http://wiki.chibios.org/dokuwiki/doku.php?id=chibios:documents#guides_and_articles" class="urlextern" target="_blank" title="http://wiki.chibios.org/dokuwiki/doku.php?id=chibios:documents#guides_and_articles"  rel="nofollow">learn more</a>
</p>
</div><div class="wrap_round wrap_box plugin_wrap">
<h4 id="need_tutorials">Need Tutorials?</h4>

<p>
Try the video tutorials and guides on Play Embedded.
</p>

<p>
<a href="http://www.playembedded.org/blog/en/category/articles/firmware-development/chibios/" class="urlextern" target="_blank" title="http://www.playembedded.org/blog/en/category/articles/firmware-development/chibios/"  rel="nofollow">learn more</a>
</p>
</div><div class="wrap_round wrap_box plugin_wrap">
<h4 id="need_support">Need Support?</h4>

<p>
The forums is the best place, registration required.
</p>

<p>
<a href="http://forum.chibios.org" class="urlextern" target="_blank" title="http://forum.chibios.org"  rel="nofollow">learn more</a>
</p>
</div>
</div>
<div class="inclmeta level0">
	&nbsp;
</div>
</div>
</div>
		</div>
	</div>
	<div id="ch_bottom">		
				<div id="ch_footer">
			
<p>
ChibiOS - Copyright &#169; 2006..2017 Giovanni Di Sirio.
</p>

		</div>
			</div>	
</div>
 <script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(["_setAccount", "UA-19330378-2"]);
			_gaq.push(["_trackPageview"]);
			(function() {
			var ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true;
			ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
			var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s);
			})();
			</script><script src="/dokuwiki/lib/tpl/chibios/js/demo/demo.js"></script>
</body>
</html>




